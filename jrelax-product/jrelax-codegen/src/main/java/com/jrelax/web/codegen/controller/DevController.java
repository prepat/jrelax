package com.jrelax.web.codegen.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by zengc on 2016-09-06.
 */
@Controller
@RequestMapping("/open/dev")
public class DevController {
    private final String TPL = "/open/dev/";

    @RequestMapping(value="index")
    public String index(Model model){
        return TPL + "index";
    }
}
