package ${pack};

import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jrelax.kit.ObjectKit;
import com.jrelax.core.web.support.WebApplicationCommon;
import com.jrelax.core.web.support.WebResult;
import com.jrelax.orm.query.PageBean;
import com.jrelax.web.support.BaseController;
import com.jrelax.core.web.transform.DataGridTransforms;
import com.jrelax.core.web.annotation.ViewPrefix;
import ${service_pack}.${className}Service;
import ${entity_pack}.${className};

/**
 * @author zengchao
 * @version 1.0
 * @since 1.0
 */
@Controller
@RequestMapping(value="${requestMapping}")
@ViewPrefix("${requestMapping}/")
public class ${className}Controller extends BaseController<${className}>{
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Resource
	private ${className}Service ${classNameLower}Service;
	
	/**
	 * 首页
	 * @param model
	 */
	@RequestMapping(method={RequestMethod.GET, RequestMethod.POST})
	public String index(Model model){
		return "index";
	}

	/**
     * 数据
     * @param pageBean
     * @return
     */
    @RequestMapping(value="/data",method={RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> data(PageBean pageBean){
        List<${className}> list = ${classNameLower}Service.list(pageBean);
        return DataGridTransforms.JQGRID.transform(list, pageBean);
    }
	
	/**
	 * 转向新增页面
	 * @param model
	 * @param pid
	 */
	@RequestMapping(value="/add",method={RequestMethod.GET})
	public String add(Model model, String pid){
		return "add";
	}
	
	/**
	 * 执行新增
	 * @param ${classNameLower}
	 * @return
	 */
	@RequestMapping(value="/add/do", method={RequestMethod.POST})
	@ResponseBody
	public JSONObject doAdd(${className} ${classNameLower}){
		try {
			//TODO 新增代码
			#foreach($!column in $!columns.entrySet())
			#set($v = $!column.value)
			#if(${v.get("name")} == "CreateTime")
			${classNameLower}.set${v.get("name")}(getCurrentTime());
			#elseif(${v.get("name")} == "CreateUser")
			${classNameLower}.set${v.get("name")}(getCurrentUser().getRealName());
			#end
			#end
			${classNameLower}Service.save(${classNameLower});
			return WebResult.success();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return WebResult.error(e.toString());
		}
	}
	
	/**
	 * 转向编辑页面
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/edit/{id}", method={RequestMethod.GET, RequestMethod.POST})
	public String edit(Model model, @PathVariable String id){
		${className} ${classNameLower} = ${classNameLower}Service.getById(id);
		if(!ObjectKit.isNotNull(${classNameLower}))
			return WebApplicationCommon.ERROR.UNAUTHORIZED_ACCESS;
		model.addAttribute("${classNameLower}", ${classNameLower});
		return "edit";
	}

    /**
	 * 执行编辑
	 * @param ${classNameLower}
	 * @return
	 */
	@RequestMapping(value="/edit/do", method={RequestMethod.POST})
	@ResponseBody
	public JSONObject doEdit(${className} ${classNameLower}){
		try {
			//TODO 校验属性
			
			//从数据库中获取最新数据
			${className} old${className} = ${classNameLower}Service.getById(${classNameLower}.getId());
			if(ObjectKit.isNull(old${className})){
				return WebResult.notAllow();
			}
			//TODO 设置需要修改的属性值
			#foreach($!column in $!columns.entrySet())
			#set($v = $!column.value)
			#if($!v.get("primary") != true)
			#if(${v.get("type")} == "boolean")
			//old${className}.set${v.get("name")}(${classNameLower}.is${v.get("name")}());
			#else
			//old${className}.set${v.get("name")}(${classNameLower}.get${v.get("name")}());
			#end
			#end
			#end
			
			${classNameLower}Service.merge(old${className});
			return WebResult.success();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return WebResult.error(e.toString());
		}
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/delete/{id}", method={RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public JSONObject delete(@PathVariable String id){
		try {
			//删除相关的所有数据
			${classNameLower}Service.delete(id);
			return WebResult.success();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return WebResult.error(e.toString());
		}
	}
	
	/**
	 * 查看详情
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/detail/{id}", method={RequestMethod.GET, RequestMethod.POST})
	public String detail(Model model, @PathVariable String id){
		${className} ${classNameLower} = ${classNameLower}Service.getById(id);
		if(!ObjectKit.isNotNull(${classNameLower}))
			return WebApplicationCommon.ERROR.UNAUTHORIZED_ACCESS;
		model.addAttribute("${classNameLower}", ${classNameLower});
		return "detail";
	}
}
