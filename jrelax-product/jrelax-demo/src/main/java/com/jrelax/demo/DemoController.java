package com.jrelax.demo;

import com.jrelax.core.web.annotation.ViewPrefix;
import com.jrelax.kit.DateKit;
import com.jrelax.web.support.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/demo")
@ViewPrefix("/demo/")
public class DemoController extends BaseController {

    /**
     * 首页
     * @param model
     * @return
     */
    @RequestMapping(value = "/index")
    public String index(Model model) {
        model.addAttribute("time", DateKit.now());
        return "index";
    }
}
